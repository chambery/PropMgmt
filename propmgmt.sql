DROP TABLE IF EXISTS contracts;
DROP TABLE IF EXISTS history;
DROP TABLE IF EXISTS leases;
DROP TABLE IF EXISTS tenants;
DROP TABLE IF EXISTS leases_tenants;
DROP TABLE IF EXISTS properties;
DROP TABLE IF EXISTS durables;

CREATE TABLE contracts (
    name          TEXT NOT NULL,
    property_id   TEXT NOT NULL,
    from_date     TEXT,
    to_date       TEXT,
    vendor        TEXT,
    website       TEXT,
    amount        INTEGER,
    paid_on       TEXT,
    notes         TEXT,
    id            INTEGER PRIMARY KEY
);


CREATE TABLE history (
    property_id   TEXT NOT NULL,
    item_name     TEXT NOT NULL,
    amount        FLOAT,
    date          TEXT NOT NULL,
    vendor        TEXT,
    notes         TEXT,
    id            INTEGER PRIMARY KEY
);


CREATE TABLE leases (
    property_id   INTEGER NOT NULL,
    from_date     TEXT NOT NULL,
    to_date       TEXT,
    deposit       INTEGER,
    id            INTEGER PRIMARY KEY
);

CREATE TABLE tenants (
    name      TEXT NOT NULL,
    phone     TEXT,
    email     TEXT,
    id        INTEGER PRIMARY KEY
);


CREATE TABLE leases_tenants (
    lease_id    INTEGER,
    tenant_id   INTEGER,
    id          INTEGER PRIMARY KEY
);


CREATE TABLE properties (
    address             TEXT NOT NULL,
    city                TEXT NOT NULL,
    state               TEXT NOT NULL,
    zip                 INTEGER NOT NULL,
    construction_year   INTEGER,
    purchase_year       INTEGER,
    id                  INTEGER PRIMARY KEY
);

CREATE TABLE durables (
    name            TEXT NOT NULL,
    make            TEXT NOT NULL,
    model           TEXT NOT NULL,
    year            INTEGER,
    purchase_year   INTEGER,
    property_id     INTEGER,
    id              INTEGER PRIMARY KEY
);

INSERT INTO contracts VALUES (1, 'insurance', '2014-07-01', '2015-07-01', 'Home Insurance Corp', '', 76000, '2014-07-15', 'some interesting things', NULL);
INSERT INTO contracts VALUES (2, 'insurance', '2014-07-01', '2015-07-01', 'Insurance Home Corp', '', 76000, '2014-07-15', 'some interesting things', NULL);

INSERT INTO history VALUES (1, 'rent', 83000.0, '2014-08-01', '', '', NULL);
INSERT INTO history VALUES (1, 'HOA', -12500.0, '2014-08-20', '', '', NULL);
INSERT INTO history VALUES (1, 'house cleaning', -32000.0, '2014-08-01', 'My Carpet Cleaning', 'Some stuff about cleaning', NULL);
INSERT INTO history VALUES (1, 'carpet cleaning', -12500.0, '2014-08-01', 'My Carpet Cleaning', 'Decent job', NULL);
INSERT INTO history VALUES (1, 'rent', 135000.0, '2014-09-17', NULL, NULL, NULL);
INSERT INTO history VALUES (1, 'rent', 135000.0, '2014-07-14', NULL, NULL, NULL);
INSERT INTO history VALUES (1, 'rent', 135000.0, '2014-06-03', NULL, NULL, NULL);
INSERT INTO history VALUES (2, 'rent', 830.0, '2014-08-01', NULL, NULL, NULL);

INSERT INTO leases VALUES (1, '2014-03-01', '2015-03-01', 190000, NULL);
INSERT INTO leases VALUES (2, '2014-08-01', '2014-10-01', 0, NULL);

INSERT INTO tenants VALUES ('Maria McDoughnut', '704-555-1212', 'mmcd@email.com', NULL);
INSERT INTO tenants VALUES ('Sean McDoughnut', '704-555-1212', 'smcd@email.com', NULL);
INSERT INTO tenants VALUES ('Bob Smith', '704-555-1212', 'bob.smith@email.com', NULL);

INSERT INTO leases_tenants VALUES (1, 1, NULL);
INSERT INTO leases_tenants VALUES (1, 2, NULL);
INSERT INTO leases_tenants VALUES (2, 2, NULL);

INSERT INTO properties VALUES ('1313 Mockingbird Ln', 'Anytown', 'NC', 28832, '1984', '2005', NULL);
INSERT INTO properties VALUES ('10 Downing St', 'Anytown', 'NC', 28832, '1932', '2000', NULL);

INSERT INTO durables VALUES ('water heater', 'Whirlpool', '323-XSS', 2009, 2009, 1, NULL);
INSERT INTO durables VALUES ('dishwasher', 'GE', 'LLDW100', 2009, 2009, 1, NULL);
INSERT INTO durables VALUES ('Air Conditioner', 'Trane', 'dsfa-vkj', 2009, 2009, 2, NULL);

